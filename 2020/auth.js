const authCookieName = "uhhwumowfjfiflcmgze";
const authCookieHashedPass = "01bb79b57ab6b942e8db6baa2d5ac53824093b08fa15f012c260a77e1e816fef";


addEventListener('fetch', event => {
  event.respondWith(handleRequest(event.request))
})

/**
 * Respond to the request
 * @param {Request} request
 */
async function handleRequest(request) {
  let isAuthenticated = await auth(request);
  if (isAuthenticated) {
    return await fetch(request);
    
  }
  else {
    return new Response("No", {status: 404})
  }
  
}


async function auth(request) {
  let ret = false;
  try {
  let authCookie = await getCookie(request, authCookieName);
  console.log(authCookie)
  let hashedAuthCookie = await digestStr(authCookie);
  if (hashedAuthCookie === authCookieHashedPass) {
    ret = true;
  }
  }
  catch(err) {
    console.log(err)
  
  }
  return ret;
}

async function getCookie(request, name) {
  let result = null
  let cookieString = request.headers.get('Cookie')
  if (cookieString) {
    let cookies = cookieString.split(';')
    cookies.forEach(cookie => {
      console.log(cookie)
      let cookieName = cookie.split('=')[0].trim()
      if (cookieName === name) {
        let cookieVal = cookie.split('=')[1]
        result = cookieVal
      }
    })
  }
  return result
}

async function digestStr(str) {
    const msgUint8 = new TextEncoder().encode(str); 
    const hashBuffer = await crypto.subtle.digest('SHA-256', msgUint8);   
    const hashArray = Array.from(new Uint8Array(hashBuffer));
    const hashHex = hashArray.map(b => b.toString(16).padStart(2, '0')).join('');
    return hashHex;
  }
  