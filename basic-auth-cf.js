
const credentialsArray = [

  {
    path: "/",
    name: "upstatement",
    pass: "sxjfrazproiexcd1gz3w5osh65h081y"
  }
];


const CREDENTIALS_REGEXP = /^ *(?:[Bb][Aa][Ss][Ii][Cc]) +([A-Za-z0-9._~+/-]+=*) *$/
const USER_PASS_REGEXP = /^([^:]*):(.*)$/
const Credentials = function(name, pass) {
  this.name = name
  this.pass = pass
}

const decodeBase64 = function(str) {
return atob(str).toString();

}

const parseAuthHeader = function(string) {
  if (typeof string !== 'string') {
    return undefined
  }
  const match = CREDENTIALS_REGEXP.exec(string)
  if (!match) {
    return undefined
  }
  // decode user pass
  const userPass = USER_PASS_REGEXP.exec(decodeBase64(match[1]))
  if (!userPass) {

    return undefined

  }
  return new Credentials(userPass[1], userPass[2])
}

const unauthorizedResponse = function(body) {
  return new Response(
    null, {
      status: 401,
      statusText: "'Authentication required.'",
      body: body,
      headers: {
        "WWW-Authenticate": 'Basic realm="User Visible Realm"'
      }
    }
  )
}
async function routeCreds(request) {
  let ret = false;
  credentialsArray.forEach(function(creds) {
    if (request.url.indexOf(creds.path) > -1) {
      ret = creds;
    }
  })
  return ret;
}
async function handle(request) {
  console.log(request);
  const credentials = parseAuthHeader(request.headers.get("Authorization"));
  let correctCreds = await routeCreds(request);
  if (correctCreds == false) {
    return unauthorizedResponse("Unauthorized");
  }
  else {
  if ( !credentials || credentials.name !== correctCreds.name ||  credentials.pass !== correctCreds.pass) {
    return unauthorizedResponse("Unauthorized")
  } else {
    return fetch(request)
  }
  }
}



addEventListener('fetch', event => {

  event.respondWith(handle(event.request))
})
addEventListener('fetch', event => {
  event.respondWith(handleRequest(event.request))
})

/**
 * Fetch and log a request
 * @param {Request} request
 */
async function handleRequest(request) {
  console.log('Got request', request)
  const response = await fetch(request)
  console.log('Got response', response)
  return response
}