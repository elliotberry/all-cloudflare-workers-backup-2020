
const config = {
  "baseURL": "https://magenic.com",
  "overridePath": "https://proxy-resources.nehima.co/magenic/",
  "overrides": 
  [
    {"path": "custom.css", "newpath": "assets/css/custom.css"},
     {"path": "custom.js", "newpath": "assets/js/custom.js"}
  ]
};

addEventListener('fetch', event => {
  event.respondWith(handleRequest(event.request))
})

/**
 * Respond to the request
 * @param {Request} request
 */
async function handleRequest(request) {
  
  let theURL = new URL(request.url);
  let resp;
  let y = await pathStuff(theURL.pathname);

  if (y == false) {
    resp = await fetch(config.baseURL + theURL.pathname);
  }
  else {
    console.log("getting " + y);
    resp = await fetch(y);
  }
  return resp;
}

async function pathStuff(thepath) {
  let overrides = config.overrides;
  let ret = "";
  if (thepath !== "/") {
  for (i=0; i < overrides.length; i++) {
      if (thepath.indexOf(overrides[i].path) > -1) {
        ret = config.overridePath + overrides[i].newpath;
      }
  }
  }
  return ret;
}