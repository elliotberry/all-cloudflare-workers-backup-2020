addEventListener('fetch', event => {
  event.respondWith(handleRequest(event.request))
});

const database = testing;

const init = {
  status: 200,
  headers: {
    "Access-Control-Allow-Origin": "*",
    "Content-Type": "application/json"
  }
};

const Method = method => req => req.method.toLowerCase() === method.toLowerCase();

const Get = Method('get');
const Post = Method('post');

const Header = (header, val) => req => req.headers.get(header) === val;
const Host = host => Header('host', host.toLowerCase());
const Referrer = host => Header('referrer', host.toLowerCase());

const Path = regExp => req => {
    const url = new URL(req.url);
    const path = url.pathname;
    const match = path.match(regExp) || [];
    return match[0] === path;
}

class Router {
    constructor() {
        this.routes = [];
    }

    handle(conditions, handler) {
        this.routes.push({
            conditions,
            handler,
        });
        return this;
    }


    get(url, handler) {
        return this.handle([Get, Path(url)], handler);
    }

    post(url, handler) {
        return this.handle([Post, Path(url)], handler);
    }
    route(req) {
        const route = this.resolve(req);

        if (route) {
            return route.handler(req);
        }

        return new Response('resource not found', {
            status: 404,
            statusText: 'not found',
            headers: {
                'content-type': 'text/plain',
            },
        });
    }
    resolve(req) {
        return this.routes.find(r => {
            if (!r.conditions || (Array.isArray(r) && !r.conditions.length)) {
                return true;
            }

            if (typeof r.conditions === 'function') {
                return r.conditions(req);
            }

            return r.conditions.every(c => c(req));
        })
    }
};
let api = {
  async getAllKeys() {
    let j = await database.list()
    return j.keys;
  },
  async putOne() {
    await database.put(obj.id, JSON.stringify(obj));
  },
  async getAll() {
    let j = await this.getAllKeys();
    let u = await Promise.all(j.map(item => this.getOne(item.name)))
    return u;
  },
  async getOne(id) {
    let obj = await database.get(id);
    return obj;
  }
}
;
async function g() {



};

async function handleRequest(request) {
  const r = new Router();
  
  r.get('/', req => api.getAll())
  r.post('/', req => api.putOne())

  let result = {
    status: "",
    data: {}
  }
  try {
    result.data = await r.route(request);
    result.status = "success";
  } catch (err) {
    result.status = "failure. Error: " + err.toString()
  }
  return new Response(JSON.stringify(result), init);
}


