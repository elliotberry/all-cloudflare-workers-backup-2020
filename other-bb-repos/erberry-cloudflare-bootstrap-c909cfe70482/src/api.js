let api = {
  async getAllKeys() {
    let j = await database.list()
    return j.keys;
  },
  async putOne() {
    await database.put(obj.id, JSON.stringify(obj));
  },
  async getAll() {
    let j = await this.getAllKeys();
    let u = await Promise.all(j.map(item => this.getOne(item.name)))
    return u;
  },
  async getOne(id) {
    let obj = await database.get(id);
    return obj;
  }
}
