const paths = {
  "/welcome": {
    "id": "welcome"
  },
  "/admin": {
    "id":"admin"
  }
};
const content = {
  "welcome": {
    "template": "post",
    "fields": {
      "title": "welcome",
      "body": "Hey hey wassup"
    }
  }
}
const templates = {
  "post": {
    "fields": [
      "title",
      "body"
    ],
    "content": `<!doctype html><html><head><title>{{title}}</title></head><body><div class="container"><p>{{body}}</p></div></body></html>`
  }
}
const admin = async function() {
  let template = `
  <!doctype html>

  <html>
    <body>
      


    </body>

  </html>
  `
}
/**
 * Example of how router can be used in an application
 *  */
addEventListener('fetch', event => {
  event.respondWith(handleRequest(event.request))
})

function handler(request) {
  const init = {
    headers: {
      'content-type': 'application/json'
    },
  }
  const body = JSON.stringify({
    some: 'json'
  })
  return new Response(body, init)
}

async function handleRequest(request) {
  const resp = router(request);
  return resp
}


async function router(req) {
  let resp;
  console.log(req);
  try {
    if (req.method == "GET") {
      let thisURL = new URL(req.url);
      console.log(thisURL)
      let contentID = paths[thisURL.pathname].id;
      resp = await formatContent(content[contentID]);
    }
  } catch (error) {
    resp = await createErrResp(error);
    
  }
  return resp;
}
async function createErrResp(e) {
  let y = e.toString();
  let t = "none";
  let status = 500;
  if (y.includes("undefined")) {
    t = "invalid path";
    status = 404;
  }
  let init = {
    "headers": {
      "Content-Type": "terxt/plain"
    },
    "status": status
  };
  return new Response("Error: " + e + " Notes: " + t, status);
}
async function formatContent(contentObj) {
  let postType = contentObj.template;
  let template = templates[postType];
  let formattedContent = await shittyHandlebars(contentObj.fields, template.content);
  let init = {
    "headers": {
      "Content-Type": "text/html"
    }
  };
  let resp = new Response(formattedContent, init);
  return resp;
}


async function shittyHandlebars(fields, template) {
  let one = template.split("{{");
  thisStr = one[0];
  one.splice(0, 1);

  one.forEach(function (item) {
    let zitem = item.split("}}");
    let fieldName = zitem[0].trim();
    if (fields[fieldName]) {
      zitem[0] = fields[fieldName];
    } else {
      zitem[0] = "";
    }
    thisStr = thisStr + zitem[0] + zitem[1];
  })
  console.log(thisStr)
  return thisStr.toString();
}