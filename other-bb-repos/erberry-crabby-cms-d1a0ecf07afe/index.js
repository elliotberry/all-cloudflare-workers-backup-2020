addEventListener('fetch', event => {
  event.respondWith(handleRequest(event.request))
})

var token; 
var values = [];

const Method = method => req =>
    req.method.toLowerCase() === method.toLowerCase()

const Get = Method('get')
const Post = Method('post')

const Header = (header, val) => req => req.headers.get(header) === val
const Host = host => Header('host', host.toLowerCase())
const Referrer = host => Header('referrer', host.toLowerCase())

const Path = regExp => req => {
    const url = new URL(req.url)
    const path = url.pathname
    const match = path.match(regExp) || []
    return match[0] === path
}

class Router {
    constructor() {
        this.routes = []
    }

    handle(conditions, handler) {
        this.routes.push({
            conditions,
            handler,
        })
        return this
    }


    get(url, handler) {
        return this.handle([Get, Path(url)], handler)
    }

    post(url, handler) {
        return this.handle([Post, Path(url)], handler)
    }
    route(req) {
        const route = this.resolve(req)

        if (route) {
            return route.handler(req)
        }

        return new Response('resource not found', {
            status: 404,
            statusText: 'not found',
            headers: {
                'content-type': 'text/plain',
            },
        })
    }
    resolve(req) {
        return this.routes.find(r => {
            if (!r.conditions || (Array.isArray(r) && !r.conditions.length)) {
                return true
            }

            if (typeof r.conditions === 'function') {
                return r.conditions(req)
            }

            return r.conditions.every(c => c(req))
        })
    }
};
//Various server functions
async function readRequestBody(request) {
  const { headers } = request
  const contentType = headers.get('content-type')
  if (contentType.includes('application/json')) {
    const body = await request.json()
    return body;
  } else if (contentType.includes('application/text')) {
    const body = await request.text()
    return body
  } else if (contentType.includes('text/html')) {
    const body = await request.text()
    return body
  } else if (contentType.includes('form')) {
    const formData = await request.formData()
    let body = {}
    for (let entry of formData.entries()) {
      body[entry[0]] = entry[1]
    }
    return JSON.stringify(body)
  } else {
    let myBlob = await request.blob()
    var objectURL = URL.createObjectURL(myBlob)
    return objectURL
  }
};
//KV GET operations - BB data
async function getAllKVKeys() {
    let j = await bbdata.list();
    return j.keys;
};
async function getAllKVValues() {
    let j = await getAllKVKeys();
    let i = j.filter(name => name.name.indexOf("{") > -1);
    let u = await Promise.all(j.map(item => getOne(item.name)))
    return u;
};

async function getOne(uuid) {
    let obj = await bbdata.get(uuid);
    try {
        return JSON.parse(obj);
    }
    catch (e) {
        return obj;
    }
}

//KV GET operations - metadata
async function getAllMetaKVKeys() {
    let j = await metadata.list()
    return j.keys;
};

async function getAllMetaKeysClean() {
    let j = await metadata.list();
   let i = j.keys.filter(name => name.name.indexOf("{") > -1);
   
   
    return i.map(function(e) { return e.name; });;
}

async function getAllMetaKVValues() {
    let j = await getAllMetaKVKeys();
    let i = j.filter(name => name.name.indexOf("{") > -1);
    let u = await Promise.all(j.map(item => getOneMeta(item.name)))
    return u;
};

async function getOneMeta(uuid) {
    let obj = await metadata.get(uuid);
    try {
        return JSON.parse(obj);
    }
    catch (e) {
        return obj;
    }
}

async function putOneMeta(request) {
    let k = await readRequestBody(request);

    await metadata.put(k.uuid, JSON.stringify(k));
    return "Updated obj, received this body data: " + JSON.stringify(k);
}
;
async function getBitbucketToken() {
    let init = {
        body: 'grant_type=client_credentials',
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'Basic ektiWHBEUEhrQmRkblZqdjdCOmdiN2NDSGZwNlJ3UGJWRXY2UnF4S3pzbWtlQ3JkdThu'
        },
    }

    let resp = await fetch('https://bitbucket.org/site/oauth2/access_token', init);
    let ret = await resp.json();
    return ret.access_token;
}
//get all directly from BB
async function getAllFresh() {
    token = await getToken();

    let d = await getRepo("https://api.bitbucket.org/2.0/repositories/erberry?pagelen=100&page=1");
    let numberOfpages = Math.ceil(d.size / d.pagelen);

    let z = [...Array(numberOfpages)].map((x, i) => (i + 1)).slice(1, numberOfpages.length);

    let u = await Promise.all(z.map(item => getRepo("https://api.bitbucket.org/2.0/repositories/erberry?pagelen=100&page=" + item)))
    u.push(d);
    let y = await format(u);
    return y;
}

//get all directly from BB
async function updateAll() {
    token = await getToken();

    let d = await getRepo("https://api.bitbucket.org/2.0/repositories/erberry?pagelen=100&page=1");
    let numberOfpages = Math.ceil(d.size / d.pagelen);

    let z = [...Array(numberOfpages)].map((x, i) => (i + 1)).slice(1, numberOfpages.length);

    let u = await Promise.all(z.map(item => getRepo("https://api.bitbucket.org/2.0/repositories/erberry?pagelen=100&page=" + item)))
    u.push(d);
    let y = await format(u);
    await Promise.all(y.map(item => updateRepoFromBitbucket(item)));
    return "yes";
}


async function format(arr) {
    let ret = [];
    arr.forEach(function (item) {
        ret.push(...item.values);
    })
    return ret;
}

async function getRepo(url) {
    let init = {
        method: 'GET',
        headers: {
            'Authorization': 'Bearer ' + token,
            'Content-Type': 'application/json'
        }
    }

    let response = await fetch(url, init);
    let ret = await response.json();
    return ret;
}
;
async function checkIfEntriesCached() {
  let lastUpdated = await lastTimeUpdated();
  let now = new Date();
  let then = Date.parse(lastUpdated);
  return now - then;
}
async function shouldWeUpdate() {
  let s = await lastTimeUpdated();
  if (s > .5) {
    return true;
  }
  else {
    return false;
  }
}
async function lastTimeUpdated() {
  let j = await bbdata.get("last-updated-kv");
  let then = Date.parse(j);
  let now = new Date();
  let diff = now - then;
  const diffDays = diff / (1000 * 60 * 60 * 24);

  return diffDays.toFixed(3);
}

async function setLastTimeUpdated() {
  let j = new Date().toISOString();
  await bbdata.put("last-updated-kv", j);
};
async function putStaticCache() {
    let j = await getAllKVValues();
    await metadata.put("static-cache", JSON.stringify(j));
    return "cool";
}

async function getStaticCache() {
    let y = await metadata.get("static-cache", "json");
    return y;
}

async function getArrayObj(arr, val) {
    var index = arr.map(function(e) { return e.name; }).indexOf('Nick');
    return index;
}


async function getAll() {
    let bb = await getStaticCache();
    let meta = await getAllMetaKeysClean();
    var k = bb.map(function(e) { return e.uuid; });
 
    await Promise.all(meta.map(async function(item) {
        let y = k.indexOf(item);
            let val = await metadata.get(item, "json");
            bb[y].meta = val;
    }));
    return bb;
};


async function handleRequest(request) {
  const r = new Router();
 // r.get('/lastupdated', req => lastTimeUpdated());
 // r.get('/shouldweupdate', req => shouldWeUpdate());
 // r.get('/updateall', req => updateAll());
 // r.get('/getall', req => getAllKVValues());
 
 // r.get('/getallcached', req => getStaticCache());
  r.get('/cache', req => putStaticCache());
 // r.get('/getallkeys', req =>  getAllKVKeys());

  r.post('/updateone', req => putOneMeta(req));


  r.get('/', req => getAll());

  let result = {
    status: "",
    data: {}
  }
  try {
    result.data = await r.route(request);
    result.status = "success";
  } catch (err) {
    result.status = "failure at handler function.";
    result.data = " Error: " + err.toString();
  }
  let init = {
    status: 200,
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Content-Type": "application/json"
    }
  };
  return new Response(JSON.stringify(result), init);
}


