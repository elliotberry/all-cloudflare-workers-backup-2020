
./src
├── cms       #CMS-specific files
│   ├── src   #CMS plumbing: Javascript functionality
│   │   └── api.js  #CMS api: post and retrieve post content.
│   └── views #CMS views: admin and internal
│       ├── admin.html  #Administration dashboard
│       └── edit.html   #Page editor.
├── index.js  #main export for the build. All files imported here.
├── lib       #shared libraries. KV/Router frameworks.
│   ├── kv.js #KV methods.
│   └── simplerouter.js #Routing class.
└── theme     #CMS front-end
    └── 2020  #theme name
        └── assets  #Static assets