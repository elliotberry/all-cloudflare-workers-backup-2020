//KV GET operations - BB data
async function getAllKVKeys() {
    let j = await bbdata.list();
    return j.keys;
};
async function getAllKVValues() {
    let j = await getAllKVKeys();
    let i = j.filter(name => name.name.indexOf("{") > -1);
    let u = await Promise.all(j.map(item => getOne(item.name)))
    return u;
};

async function getOne(uuid) {
    let obj = await bbdata.get(uuid);
    try {
        return JSON.parse(obj);
    }
    catch (e) {
        return obj;
    }
}

//KV GET operations - metadata
async function getAllMetaKVKeys() {
    let j = await metadata.list()
    return j.keys;
};

async function getAllMetaKeysClean() {
    let j = await metadata.list();
   let i = j.keys.filter(name => name.name.indexOf("{") > -1);
   
   
    return i.map(function(e) { return e.name; });;
}

async function getAllMetaKVValues() {
    let j = await getAllMetaKVKeys();
    let i = j.filter(name => name.name.indexOf("{") > -1);
    let u = await Promise.all(j.map(item => getOneMeta(item.name)))
    return u;
};

async function getOneMeta(uuid) {
    let obj = await metadata.get(uuid);
    try {
        return JSON.parse(obj);
    }
    catch (e) {
        return obj;
    }
}

async function putOneMeta(request) {
    let k = await readRequestBody(request);

    await metadata.put(k.uuid, JSON.stringify(k));
    return "Updated obj, received this body data: " + JSON.stringify(k);
}
