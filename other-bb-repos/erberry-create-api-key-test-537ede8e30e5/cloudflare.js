addEventListener('fetch', event => {
    event.respondWith(handleRequest(event.request))
})


async function handleRequest(request) {
    var ret;
    if (request.headers.get("cookie")) {
        var hashedPass = "c44b9e911d3fadd76a24288a6974c9a555522085d26ac2a4f6c271e99fb7c550";
        let incomingPass = await getPass(request.headers.get("cookie"));
        var hashIPass = d(incomingPass);
        var hashIPasst = hexString(hashIPass);

        if (hashIPasst == hashedPass) {
            stat = 200;
            const ret = "cool"; //await fetch(request);
        } else {
            stat = 404;
            ret = "";
        }
    } else {
        stat = 404;
        ret = "";
    }
    return new Response(ret, {
        status: stat
    })
}

function getPass(cookie) {
    var y = cookie.split("auth-lol");
    var u = y[1];
    var b = u.split("=");
    return b[1];
}

function hexString(buffer) {
    const byteArray = new Uint8Array(buffer);

    const hexCodes = [...byteArray].map(value => {
        const hexCode = value.toString(16);
        const paddedHexCode = hexCode.padStart(2, '0');
        return paddedHexCode;
    });
    return hexCodes.join('');
}

let d = function (data) {
    const encoder = new TextEncoder();
    const dr = encoder.encode(data);
    return crypto.subtle.digest("SHA-256", dr);
}