
//const uuidv4 = require('uuid/v4');
//var uuid = uuidv4();


const {createHash} = require('crypto');
var randomstring = require("randomstring");
var pass = randomstring.generate(200);


console.log(pass);

// lines: array of strings
function computeSHA256(lines) {
  const hash = createHash('sha256');
  for (let i = 0; i < lines.length; i++) {
    const line = lines[i].trim(); // remove leading/trailing whitespace
    if (line === '') continue; // skip empty lines
    hash.write(line); // write a single line to the buffer
  }

  return hash.digest('base64'); // returns hash as string
}

console.log(computeSHA256(pass));