var extraDebub = "";
const Method = (method) => (req) =>
  req.method.toLowerCase() === method.toLowerCase()

const Get = Method('get')
const Post = Method('post')

const Header = (header, val) => (req) => req.headers.get(header) === val
const Host = (host) => Header('host', host.toLowerCase())
const Referrer = (host) => Header('referrer', host.toLowerCase())

const Path = (regExp) => (req) => {
  const url = new URL(req.url)
  const path = url.pathname
  const match = path.match(regExp) || []
  return match[0] === path
}

class Router {
  constructor() {
    this.routes = []
  }

  handle(conditions, handler) {
    this.routes.push({
      conditions,
      handler,
    })
    return this
  }

  get(url, handler) {
    return this.handle([Get, Path(url)], handler)
  }

  post(url, handler) {
    return this.handle([Post, Path(url)], handler)
  }
  route(req) {
    const route = this.resolve(req)

    if (route) {
      return route.handler(req)
    }

    return new Response('resource not found', {
      status: 404,
      statusText: 'not found',
      headers: {
        'content-type': 'text/plain',
      },
    })
  }
  resolve(req) {
    return this.routes.find((r) => {
      if (!r.conditions || (Array.isArray(r) && !r.conditions.length)) {
        return true
      }

      if (typeof r.conditions === 'function') {
        return r.conditions(req)
      }

      return r.conditions.every((c) => c(req))
    })
  }
}

//Various server functions
async function readRequestBody(request) {
  const { headers } = request
  const contentType = headers.get('content-type')
  if (contentType.includes('application/json')) {
    const body = await request.json()
    return body;
  } else if (contentType.includes('application/text')) {
    const body = await request.text()
    return body
  } else if (contentType.includes('text/html')) {
    const body = await request.text()
    return body
  } else if (contentType.includes('form')) {
    const formData = await request.formData()
    let body = {}
    for (let entry of formData.entries()) {
      body[entry[0]] = entry[1]
    }
    return body
  } else {
    let myBlob = await request.blob()
    var objectURL = URL.createObjectURL(myBlob)
    return objectURL
  }
}

async function putOne(key, data) {

 await db.put(key, JSON.stringify(data))
  return `Updated this object: ${JSON.stringify(data)}`
}
async function getAllKeys() {
  let j = await db.list()
  return j.keys
}

async function getAll() {
  let j = await getAllKeys()
  let u = await Promise.all(j.map((item) => getOne(item.name)))
  const sortedArr = u.sort((a, b) => {
    let datea = new Date(a.date)
    let dateb = new Date(b.date)
    datea -dateb
  })
  return sortedArr
}

async function getOne(uuid) {
  let obj = await db.get(uuid)
  try {
    return JSON.parse(obj)
  } catch (e) {
    return obj
  }
}

async function toHex(str) {
  var result = '';
  for (var i=0; i<str.length; i++) {
    result += str.charCodeAt(i).toString(16);
  }
  return result;
}


async function postReq(request) {

  let k = await readRequestBody(request);
  let theDate = new Date()
  let theDateStr = theDate.toLocaleString()
  let key = await toHex(theDateStr)
  if (k) {
    let newObj = {
      reqBody: k,
      date:  theDateStr,
      key: key
    }
    return await putOne(key, newObj);

  }
  else {
    return "ya didn't send data, asshole"
 
  }

 
 
}


async function handleRequest(request) {
  const r = new Router()

  r.get('/',  async function(req) { return await getAll()})
  r.post('/', async function(req) { return await postReq(req)})
  let result = {
    status: '',
    data: {}
  }
  try {
    result.data = await r.route(request)
    result.status = 'success'
  } catch (err) {
    result.status = 'failure at handler function.'
    result.data = ' Error: ' + err.toString()
  }
  let init = {
    status: 200,
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Content-Type': 'application/json',
    },
  }
  return new Response(JSON.stringify(result), init)
}

addEventListener('fetch', (event) => {
  event.respondWith(handleRequest(event.request))
})
