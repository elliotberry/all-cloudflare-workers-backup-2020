addEventListener('fetch', event => {
  event.respondWith(handleRequest(event.request))
})
var token; 
var values = [];
/**
 * Respond to the request
 * @param {Request} request
 */
async function handleRequest(request) {
  token = await getToken();

    let d = await getRepo("https://api.bitbucket.org/2.0/repositories/erberry?pagelen=100&page=1");
    let numberOfpages = Math.ceil(d.size / d.pagelen);
    
  let z = [...Array( numberOfpages )].map( (x, i) => (i + 1)).slice(1,numberOfpages.length);
  
  let u = await Promise.all(z.map(item => getRepo("https://api.bitbucket.org/2.0/repositories/erberry?pagelen=100&page=" + item)))
  u.push(d);
  let y = await format(u);
  return new Response(JSON.stringify(y), {status: 200});
}

async function format(arr) {
  let ret = [];
  arr.forEach(function(item) {
    ret.push(...item.values);
  })
  return ret;
}
async function getToken() {
  let init = {
    body: 'grant_type=client_credentials',
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Basic ektiWHBEUEhrQmRkblZqdjdCOmdiN2NDSGZwNlJ3UGJWRXY2UnF4S3pzbWtlQ3JkdThu'
    },
  }

  let resp = await fetch('https://bitbucket.org/site/oauth2/access_token', init);
  let ret = await resp.json();
  return ret.access_token;
}

async function getRepo(url) {
  let init = {
    method: 'GET',
    headers: {
     'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json'
      }
  }

  let response = await fetch(url, init);
  let ret = await response.json();
  return ret;
}
