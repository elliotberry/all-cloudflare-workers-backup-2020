
  const responseInit = {
      headers: {'Content-Type': 'application/json'}
    }
    let combined =     {
    "key": "bb-fucking-around",
    "name": "bottom text",
    "description": "An example app for Bitbucket",
    "vendor": {
        "name": "nice nerds",
        "url": "https://www.example.com"
    },
    "baseUrl": "https://bb.daddy.workers.dev",
    "authentication": {
        "type": "none"
    }
    }


addEventListener('fetch', event => {
  event.respondWith(fetchAndApply(event.request))
})

/**
 * Making a curl request that looks like
 * curl -X POST --data 'key=world' example.com
 * or
 * curl -X POST --form 'key=world' example.com
 */
async function fetchAndApply(request) {
  try {
    const postData = await request.formData();
    return new Response(`hello ${postData.get('key')}`)
  } catch (err) {
    return new Response(JSON.stringify(combined), responseInit)
  }
}
/**
 * Respond to the request
 * @param {Request} request
 * CLIENT ID
xZAS2TXBS9jMZE9U2TeXqaQ86t4ue7yM
SECRET
SvzDBA+g4wZ/snzH+Yzf9PlFoHv7CmkCSabP4loe80I

 */
