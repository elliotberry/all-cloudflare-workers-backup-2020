addEventListener('fetch', event => {
  event.respondWith(handleRequest(event.request))
})

/**
 * Respond to the request
 * @param {Request} request
 */
async function handleRequest(request) {

 let r = request.url.split('?')[1];

  let y = await fetch("https://pastebin.com/raw/" + r, { cf: { cacheEverything: true } })
  let o = await y.body
  return new Response(o)
}