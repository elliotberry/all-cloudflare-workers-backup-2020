addEventListener('fetch', event => {
  event.respondWith(handleRequest(event.request))
})

async function handleRequest(request) {
  setCookie()
  return new Response("j", {status: 200})
}


async function setCookie() {
    let u = new Date();
  u.setDate(u.getDate() + 1);
  let cookieDate = u.toLocaleString();
  var y = await cryptFunc.encrypt(cookieDate);
 var b = await cryptFunc.decrypt(y);
  console.log(b);
  console.log(y);
   const str = "auth-nehima=" + y;
 
 
  //response = new Response(response.body, response)
  //response.headers.set('Set-Cookie', randomStuff)
}







var cryptFunc = {
  ivvv: new Uint8Array([101, 71, 132, 220, 165, 169, 244, 179, 201, 42, 118, 136]),
strToArrayBuffer: function(str) {
  var buf = new ArrayBuffer(str.length * 2);
  var bufView = new Uint16Array(buf);
  for (var i = 0, strLen = str.length; i < strLen; i++) {
    bufView[i] = str.charCodeAt(i);
  }
  return buf;
},
arrayBufferToString: function(buf) {
  return String.fromCharCode.apply(null, new Uint16Array(buf));
},
hexEncode: function(str){
    var hex, i;

    var result = "";
    for (i=0; i<str.length; i++) {
        hex = str.charCodeAt(i).toString(16);
        result += ("000"+hex).slice(-4);
    }

    return result
},
hexDecode: function(str){
    var j;
    var hexes = str.match(/.{1,4}/g) || [];
    var back = "";
    for(j = 0; j<hexes.length; j++) {
        back += String.fromCharCode(parseInt(hexes[j], 16));
    }

    return back;
},
  encrypt: async function(msg) {
      let cryptKey = await this.getKey();
     
      let theText = this.strToArrayBuffer(msg);

      let theCrypt = await crypto.subtle.encrypt({
              name: "AES-GCM",
              iv: this.ivvv
          },
          cryptKey,
          theText
      );
      
      let yyy = await this.arrayBufferToString(theCrypt);
   
      return this.hexEncode(yyy);
  },
  decrypt: async function(ciphertexthex) {
    let t = await this.hexDecode(ciphertexthex);
    let cryptKey = await this.getKey();
    let decodedBuf = await crypto.subtle.decrypt(
      {
        name: "AES-GCM",
        iv: this.ivvv
      },
      cryptKey,
      t
    );
    let uuu = await arrayBufferToString(decodedBuf);
    return uuu;
  },

  getKey: async function() {
      const keyData = new Uint8Array([173,138,57,74,237,15,253,219,65,164,69,228,255,67,65,168,106,12,78,9,68,119,162,214,241,253,92,144,3,93,148,167]);

      let result = crypto.subtle.importKey(
          "raw",
          keyData,
          "AES-GCM",
          true,
          ["encrypt", "decrypt"]
      );
      return result;
  }
}