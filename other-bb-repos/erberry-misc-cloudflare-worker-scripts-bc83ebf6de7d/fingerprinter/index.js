addEventListener('fetch', event => {
  event.respondWith(handleRequest(event.request))
})

async function handleRequest(request) {
 
  let r = await fetch("https://obfuscator.io/obfuscate", {
    body: "${\"code\":\"// Paste your JavaScript code here\\nfunction hi() {\\\\n  console.log(\\\\\"Hello World\\u0021\\\\\");\\\\n}\\\\nhi();\",\"options\":{\"compact\":true,\"selfDefending\":false,\"disableConsoleOutput\":false,\"debugProtection\":true,\"debugProtectionInterval\":true,\"stringArray\":true,\"rotateStringArray\":true,\"rotateStringArrayEnabled\":true,\"stringArrayThreshold\":0.8,\"stringArrayThresholdEnabled\":true,\"stringArrayEncoding\":\"false\",\"stringArrayEncodingEnabled\":true,\"sourceMap\":false,\"sourceMapMode\":\"off\",\"sourceMapBaseUrl\":\"\",\"sourceMapFileName\":\"\",\"sourceMapSeparate\":false,\"domainLock\":[],\"reservedNames\":[],\"reservedStrings\":[],\"seed\":0,\"controlFlowFlatteningThreshold\":0.75,\"controlFlowFlattening\":false,\"deadCodeInjectionThreshold\":0.4,\"deadCodeInjection\":false,\"unicodeEscapeSequence\":false,\"renameGlobals\":false,\"target\":\"browser\",\"identifierNamesGenerator\":\"hexadecimal\",\"identifiersPrefix\":\"\",\"transformObjectKeys\":false,\"hydrated\":true}}",
    headers: {
      Accept: "application/json",
      "Cache-Control": "no-cache",
      "Content-Type": "application/json",
      Dnt: "1",
      Pragma: "no-cache",
      "Sec-Ch-Ua": "Google Chrome 76",
      "Sec-Fetch-Dest": "empty",
      "Sec-Fetch-Mode": "cors",
      "Sec-Fetch-Site": "same-origin"
    },
    method: "POST"
  })
 
  let y = await r.json();
  
  const response = new Response (JSON.stringify(y.code));
  return response
}