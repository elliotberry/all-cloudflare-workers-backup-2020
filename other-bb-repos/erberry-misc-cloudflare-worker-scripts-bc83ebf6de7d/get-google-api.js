addEventListener('fetch', event => {
  event.respondWith(handleRequest(event))
})

let cache = caches.default

/**
 * Respond to the request
 * @param {Request} request
 */
async function handleRequest(event) {
  let t = await cache.match(event.request)

  if (!t) {
     let t = await y("https://script.googleusercontent.com/macros/echo?user_content_key=fy_qE1HZK0qjBp5CM459oYDRVqyAwtSP7yqm24NMtpNl1aacm17UHFQZs793eg364wHiVcYVK-6fU77puC_IVF5s_NBPiqOSOJmA1Yb3SEsKFZqtv3DaNYcMrmhZHmUMWojr9NvTBuBLhyHCd5hHa5V7SzAZj2xBfFDRtNxpfsmuqfjnOYLBpWrI3G8IWJh29l4LSossvEa_fiNHZ0znxEBErwHi9mmilUSxpAgAMgCl2yodBjaH8zWF4e5U2QkSlRKuraWEfTpGJOoURM6W9auM2X845wZnwqJbeTowOR9DAwSPoPH6yLTbeyvXIw74VxFV3cZCato&lib=M7OO09pfGNQD9igEAo4bouJoiE_6Oxspk")
     var u = new Response(JSON.stringify(t), {status: 200})
    event.waitUntil(cache.put(event.request, u))
  }


  return u;
}


let y = async function (url) {
let g = await fetch(url)
    let m = await g.json()
    return m;
}

