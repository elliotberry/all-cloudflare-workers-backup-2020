

addEventListener('fetch', event => {
    event.respondWith(getSheet(event.request))
})
  


async function getSheet(request) {

    const sheet = function() {
      return fetch('https://script.google.com/macros/s/AKfycbzGvKKUIaqsMuCj7-A2YRhR-f7GZjl4kSxSN1YyLkS01_CfiyE/exec?id=15LuaN-qvPW86cGfKPXJdNEvQjpFQjNreq0O4SxI_-AY&sheet=Sheet1&header=2&startRow=3')
    }
  
    const s = await sheet()
    const sht = await s.json()
  
  
  
    const responseInit = {
      headers: {'Content-Type': 'application/json'}
    }
    return new Response(JSON.stringify(sht), responseInit)
}

