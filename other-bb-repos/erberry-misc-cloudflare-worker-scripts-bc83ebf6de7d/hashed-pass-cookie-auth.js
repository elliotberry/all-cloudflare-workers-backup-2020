

var hashedPass = "114b8de9b003eff3b8ab896810bb8a45f8c7698919ed55a20f3c76cfe9ed12e1";
addEventListener('fetch', event => {
  event.respondWith(handleRequest(event.request))
})


async function handleRequest(request) {
  
  let cookies = request.headers.get("Cookie") || ""
  if (cookies.includes("auth-lol")) {
  var ret;
  
  let incomingPass = await getPass(cookies);
  console.log(incomingPass)
  var hashIPass = d(incomingPass);
  var hashIPasst = hexString(hashIPass);

  if (hashIPasst == hashedPass) {
    stat = 200;
    ret = "yes"; 
  }
  }
    else {
    stat = 404;
    ret = "no";
  }
 
  return new Response(ret, {status: stat})
}

function getPass(cookie) {
 
    var y = cookie.split("auth-lol");
    var u = y[1];
    var b = u.split("=");
    return b[1];
  
}

function hexString(buffer) {
  const byteArray = new Uint8Array(buffer);

  const hexCodes = [...byteArray].map(value => {
    const hexCode = value.toString(16);
    const paddedHexCode = hexCode.padStart(2, '0');
    return paddedHexCode;
  });
  return hexCodes.join('');
}

let d = function(data) {
  const encoder = new TextEncoder();
  const dr = encoder.encode(data);
  return crypto.subtle.digest("SHA-256", dr);
}