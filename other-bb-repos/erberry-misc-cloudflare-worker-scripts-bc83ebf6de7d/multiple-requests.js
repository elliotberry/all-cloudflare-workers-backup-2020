addEventListener('fetch', event => {
    event.respondWith(fetchAndApply(event.request))
})
  
/**
 * Make multiple requests, 
 * aggregate the responses and 
 * send it back as a single response
 */
async function fetchAndApply(request) {

    let arr = ["https://my-json-server.typicode.com/typicode/demo/posts", "https://my-json-server.typicode.com/typicode/demo/posts"];


  const d = async function(url) {
    let r = await fetch(url);
    return await r.json();
  }

    const dad = await Promise.all(
  arr.map(async function(url) {
        return d(url);
   })
);

  
   
    return new Response(JSON.stringify(dad))
}