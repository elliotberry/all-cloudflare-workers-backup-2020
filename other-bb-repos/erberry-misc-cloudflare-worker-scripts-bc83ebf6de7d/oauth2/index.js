addEventListener('fetch', event => {
  event.respondWith(parseRequest(event.request))
})


async function parseRequest(request) {
  const url = new URL(event.request.url); //Url requested
  let userLoggedIn = await checkToken(request);
  let resp;
  if (userLoggedIn) {
    resp = await go(request);
  }
  else {
    resp = await loginPage(request);
  }
  return resp;
}

async function loginPage() {
  let page = `
  <html>
<body>

<form>
dqwdqwfqwfqwfqwf
</form>
</body>

  </html>
  `;
return new Response(page);
}

let checkToken = function(request) {
  let cookie = request.headers.get('cookies');
  if (cookie {
    
  })
}


async function requestLogin(request) {

}


async function giveLoginPageResponse(request: Request) {
  //...checks for cases where I am not necessarily logged in... 
  let token = getTokenFromRequest(request)
  if (token) { //user already signed in
    return new Response(giveAcceptPage(request)
    }
  }
}

async function redirectCodeToConsumer(request: Request) {
  let session = await verifyUser(request)

  if (session.msg == "403") return new Response(give403Page(), {
    status: 403
  })
  if (session.msg == "dne") return registerNewUser(session.email, session.pwd)
  let code = Math.random().toString(36).substring(2, 12)
  try {
    let req_url = new URL(request.url)
    let redirect_uri = new URL(encodeURI(req_url.searchParams.get("redirect_uri")))
    let client_id = new URL(encodeURI(req_url.searchParams.get("client_id")))
    // @ts-ignore
    await CODES.put(client_id + email, code)
    redirect_uri.searchParams.set("code", code);
    redirect_uri.searchParams.set("response_type", "code");
    return Response.redirect(redirect_uri.href, 302);
  } catch (e) {
    // @ts-ignore
    await CODES.delete(email, code)
    return new Response(
      JSON.stringify(factoryIError({
        message: "error with the URL passed in" + e
      })), {
        status: 500
      });
  }
}


async function giveToken() {
  headers.append("set-cookie", "token=Bearer " + tokenJWT);
  // @ts-ignore
  await TOKENS.put(email, tokenJWT)
  var respBody = factoryTokenResponse({
    "access_token": tokenJWT,
    "token_type": "bearer",
    "expires_in": 2592000,
    "refresh_token": token,
    "token": token
  })
} else {
  respBody.errors.push(factoryIError({
    message: "there was no code sent to the authorize token url"
  }))
}
return new Response(JSON.stringify(respBody), {
  headers
});
}


async function giveResource(request: Request) {
  var respBody: HookResponse = factoryHookResponse({})
  let token = ""
  let decodedJWT = factoryJWTPayload()
  try { //validate request is who they claim
    token = getCookie(request.headers.get("cookie"), "token")
    if (!token) token = request.headers.get("Authorization").substring(7)
    decodedJWT = jwt.verify(token, credentials.storage.secret)
    // @ts-ignore
    let storedToken = await TOKENS.get(decodedJWT.sub)
    if (isExpired(storedToken)) throw new Error("token is expired") /* TODO instead of throwing error send to refresh */
    if (storedToken != token) throw new Error("token does not match what is stored")
  } catch (e) {
    respBody.errors.push(factoryIError({
      message: e.message,
      type: "oauth"
    }))
    return new Response(JSON.stringify(respBody), init)
  }
  respBody.body = getUsersPersonalBody(decodedJWT.sub)
  return new Response(JSON.stringify(respBody), init)
}