
const JavaScriptObfuscator = require('javascript-obfuscator');

addEventListener('fetch', event => {
  event.respondWith(handleRequest(event.request))
})

/**
 * Fetch and log a request
 * @param {Request} request
 */
async function handleRequest(request) {
  let res;
  if (request.url.split("?")[1] && request.url.split("?")[1].indexOf("js") > -1) {
    let f = await fetch(request.url.split("?")[1]);
    let r = await f.body
    let k = await obfu(r);
    res = k;
  }
  else {
    res = "no"
  }
  let init = {
    headers: {
      "Content-Type": "text/javascript;charset=UTF-8"
    },
    status: 200
  }
  return new Response(res, init)
}


async function obfu(data) {
  var obfuscationResult = JavaScriptObfuscator.obfuscate(data);
  return obfuscationResult;
}