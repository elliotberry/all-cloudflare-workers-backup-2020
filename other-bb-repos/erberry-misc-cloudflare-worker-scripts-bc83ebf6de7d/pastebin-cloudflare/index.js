const Router = require('./router')

//Entrypoint
addEventListener('fetch', event => {
    event.respondWith(handleRequest(event.request))
})


//Router
async function handleRequest(request) {
    const r = new Router()
    // Replace with the approriate paths and handlers
    r.get('.*/get/*', () => handlerGet(req))
    r.post('.*/upload', req => handlerPost(req))


    r.get('/', () => new Response('Hello worker!')) // return a default message for the root route

    const resp = await r.route(request)
    return resp
}

//
// handlers
//

function handlerGet(request) {
    const init = {
        headers: {
            'content-type': 'application/json'
        },
    }
    const body = JSON.stringify({
        some: 'json'
    })
    return new Response(body, init)
}

async function handlerPost(request) {
    let m = await request.body;


    let encryptedData = await encryptMessage(m);

    return new Response(encryptedData)
}


//Get secrets from KV
async function getSecrets(key) {
    //const value = await SECRETS.get("pastebin-key")
    return "420blazeitlol"
}



//Crypto

async function encryptMessage(msg) {
    let cryptKey = await getKey(); //await getSecrets("aes-key");
    let enc = new TextEncoder();
    let encoded = enc.encode(msg);
    iv = crypto.getRandomValues(new Uint8Array(12));
    return {"data": crypto.subtle.encrypt({
            name: "AES-GCM",
            iv: iv
        },
        cryptKey,
        encoded
    ), "iv": iv};
}

async function decryptMessage(data, iv) {
    let cryptKey = await getKey();
    return crypto.subtle.decrypt(
        {
            name: "AES-GCM",
            iv: ArrayBuffer(12)
        },
        cryptKey, 
        data //ArrayBuffer of the data
    )
}


async function getKey() {
    const keyData = new Uint8Array([173,138,57,74,237,15,253,219,65,164,69,228,255,67,65,168,106,12,78,9,68,119,162,214,241,253,92,144,3,93,148,167]);

    let result = crypto.subtle.importKey(
        "raw",
        keyData,
        "AES-GCM",
        true,
        ["encrypt", "decrypt"]
    );
    return result;
}