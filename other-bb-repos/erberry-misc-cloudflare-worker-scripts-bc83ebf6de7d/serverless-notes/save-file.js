var express = require('express');
var app = express();
var port = 4040;
var fs = require('fs');
var bodyParser = require('body-parser')
var blake2 = require('blake2');
let pubDir = './public';
let url ="http://localhost:4040/";


//app.use(bodyParser.json({ type: 'application/*+json' }))
app.use('/', express.static(pubDir))
app.post('/upload', function(req, res) {
  let finalName;
  var name = randStr();
  var writeStream = fs.createWriteStream(pubDir + "/" + name + '.txt');
  
  req.on('data', function (data) {
    console.log("data");
    var h = blake2.createHash('blake2b', {digestLength: 16});
    h.update(new Buffer(data));
    finalName = h.digest("hex"); 
  });

  // This pipes the POST data to the file
  req.pipe(writeStream);

  // After all the data is saved, respond with a simple html form so they can post more data
  req.on('end', function () {
    console.log("end");
    rn(name, finalName);
    res.writeHead(200, {"content-type":"text/html"});
    res.end('{"fileLoc": ' + url + finalName + '.txt"}');
  });

  // This is here incase any errors occur
  writeStream.on('error', function (err) {
    console.log(err);
  });
});

//Rename temp to hash
function rn(name, newName) {
  fs.rename(pubDir + "/" + name + ".txt", pubDir + "/" + newName + ".txt", function(err) {
    if ( err ) console.log('ERROR: ' + err);
});
}
function randStr() {
  return Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
}
app.listen(port, function() {
  console.log('server up and running at port: %s', port);
});
