
addEventListener('fetch', event => {
  event.respondWith(handleRequest(event.request))
})


async function handleRequest(request) {
  
  let cookies = request.headers.get('Cookie') || ""
  if (cookies.includes("AlHw7pBejUuXpZtsIS210pj5C87yATxQ8WctIILkFBUqd6BgCtDznKpyZs1KpRESZZnbqEzZRrejwODEHhlHoL0HDrg9NESSVxOPuGC2deJ4b7m4xlFiN2C0LfE5bGVGsIcPIPuVd7d1LDLcT3dtHLSveSvdLi90PmToH471rVSvJux8QDPHZXj2LO50n6kQL29dvDNRVMLkGc0dxYnI8gNqooBTqQT8ujDPwT3DGfH5fCL9P0EwZAoZOE")) {


   return fetch(request)
  }
  else {
    return new Response("", {status: "404"})
  }
}
