addEventListener('fetch', event => {
  event.respondWith(handleRequest(event.request))
})

async function handleRequest(request) {
  var y = await cryptFunc.encrypt("msg")
  var j = await cryptFunc.decrypt(y)
  return new Response(j, {status: 200})
}

var cryptFunc = {
  ivvv: new Uint8Array([101, 71, 132, 220, 165, 169, 244, 179, 201, 42, 118, 136]),

  encrypt: async function(msg) {
      let cryptKey = await this.getKey();
      let enc = new TextEncoder();
      let encoded = enc.encode(msg);

      return crypto.subtle.encrypt({
              name: "AES-GCM",
              iv: this.ivvv
          },
          cryptKey,
          encoded
      );
  },
  decrypt: async function(ciphertext) {
    let cryptKey = await this.getKey();
    return crypto.subtle.decrypt(
      {
        name: "AES-GCM",
        iv: this.ivvv
      },
      cryptKey,
      ciphertext
    );
  },

  getKey: async function() {
      const keyData = new Uint8Array([173,138,57,74,237,15,253,219,65,164,69,228,255,67,65,168,106,12,78,9,68,119,162,214,241,253,92,144,3,93,148,167]);

      let result = crypto.subtle.importKey(
          "raw",
          keyData,
          "AES-GCM",
          true,
          ["encrypt", "decrypt"]
      );
      return result;
  }
}