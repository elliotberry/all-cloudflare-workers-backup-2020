const eslint = require('gulp-eslint');
var exec = require('child_process').exec;
var fileinclude = require('gulp-file-include');

var gulp = require('gulp');

function scripts() {
  return gulp.src(['./src/index.js'])
  .pipe(fileinclude({
    prefix: '@@',
    basepath: '@file'
  }))
  .pipe(gulp.dest('./'));
};

function lint() {
  return gulp.src(['./index.js'])
  .pipe(eslint())
  // eslint.format() outputs the lint results to the console.
  // Alternatively use eslint.formatEach() (see Docs).
  .pipe(eslint.format())
  // To have the process exit with an error code (1) on
  // lint error, return the stream and pipe to failAfterError last.
  .pipe(eslint.failAfterError());
}

function update(cb) {
    exec('wrangler publish', function (err, stdout, stderr) {
      console.log(stdout);
      console.log(stderr);
      cb(err);
    });
  };


function watchFiles() {
  gulp.watch("./src/*.js", build);

}
const build = gulp.series(scripts, update);
exports.watch = watchFiles;
exports.default = gulp.parallel(watchFiles, build);
