let api = {
  async getAllKeys() {
    let j = await steel.list()
    return j.keys;

  },
  async putOne() {
    //await KV.put(obj.id, JSON.stringify(obj));
    //return req.method;
  },
  async getAll() {
    let j = await getAllKVKeys();
    let u = await Promise.all(j.map(item => getOneKV(item.name)))
    return u;
  },
  async getOne(id) {
    let obj = await steel.get(id);
    return obj;
  }
}
