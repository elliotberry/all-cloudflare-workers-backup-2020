addEventListener('fetch', event => {
  event.respondWith(handleRequest(event.request))
})

@@include('./simplerouter.js');
@@include('./api.js');

async function handleRequest(request) {
  const r = new Router();
  
  r.get('/', req => api.getAll())
  r.post('/', req => api.putOne())

  let result = {
    status: "",
    data: {}
  }
  try {
    result.data = await r.route(request);
    result.status = "success";
  } catch (err) {
    result.status = "failure at " + fn.name + ". Error: " + err.toString()
  }
  let init = {
    status: 200,
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Content-Type": "application/json"
    }
  };
  return new Response(JSON.stringify(result), init);
}


