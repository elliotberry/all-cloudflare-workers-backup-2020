async function handleRequest(request) {
  const init = {
    headers: {
      'content-type': 'text/html;charset=UTF-8',
    },
  }
  let t = new URL(request.path);
  console.log(t);
  //const response = await fetch(url, init)
  //const results = await gatherResponse(response)
  return new Response(results, init)
}

addEventListener('fetch', event => {
  return event.respondWith(handleRequest(event.request))
})
