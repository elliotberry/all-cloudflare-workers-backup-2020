const proxy = require('./lib/proxy')
addEventListener('fetch', event => {
    event.respondWith(proxy(event.request));
})