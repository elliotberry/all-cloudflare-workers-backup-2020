const auth = require("./auth");
const loggit = require('./log');
// List of domains bind to your WorkersProxy.
const pathList = [
  {
    host: "concourse.s0.rs",
    realURL: "http://199.241.136.27:8080",
    auth: true,
  },
];

addEventListener("fetch", (event) => {
  event.respondWith(fetchAndApply(event.request));
});

async function checkPaths(request) {
  let ret = null;
  let url = request.url;
  let y = new URL(request.url);

  for await (path of pathList) {
    if (path.host == y.host) {
      if (path.auth === true) {
        let isAuthenticated = await auth(request);
        if (isAuthenticated === true) {
          y.host = path.realURL;
          ret = new URL(path.realURL + y.pathname);
        }
      } else {
        y.host = path.realURL;
        ret = new URL(path.realURL + y.pathname);
      }
    }
  }
  return ret;
}

async function fetchAndApply(request) {
await loggit(request);
  let response = null;
  let newURL = await checkPaths(request);
  if (newURL !== null) {
    let method = request.method;
    let headers = request.headers;
    response = await fetch(url, {
      method: method,
      headers: headers,
    });
  } else {
    response = new Response("No", { status: 404 });
  }

  return response;
}
